
***Hike Between Parks***

 - A 4 point hike, and waypoint.
 - A 5 point weekend workout 

![***Hike between Parks***](https://drive.google.com/uc?view=export&id=0BzzClW0hvLSPV1F5TDJzTXJuTGc)

***Marymoor Park***
![Marymoor Park](https://drive.google.com/uc?view=export&id=0BzzClW0hvLSPQlRKLUN2VURzTjA)


***Layers and Steps on Mobile***
![layers and locations](https://drive.google.com/uc?view=export&id=0BzzClW0hvLSPck40TTZRTFAzTXM)


***Mobile Routes***
![Mobile Routes](https://drive.google.com/uc?view=export&id=0BzzClW0hvLSPUDZFTnM4eEZucEE)

